import Vue from 'vue'
import {mapGetters, mapState} from 'vuex'

import App from './app'
import router from './router'
import store from './store'

import handle_service_worker from './service-worker-handlers'

import Notifications from 'vue-notification'

import services from './services'

import './plugins/vuetify'
import FacebookSDK from './plugins/facebook_sdk'
import GoogleMapSDK from './plugins/google_map_sdk'
import GoogleApiSDK from './plugins/google_api_sdk'
import Firebase from './plugins/firebase'
import translations from '@/assets/translations'

handle_service_worker(store)

Vue.config.productionTip = false
Vue.prototype.$trans = translations

Vue.use(services)
Vue.use(Notifications)
Vue.use(FacebookSDK)
Vue.use(GoogleMapSDK)
Vue.use(GoogleApiSDK)
Vue.use(Firebase)

router.beforeEach((to, _from, next) => {
  const access_token = store.state.User.access_token
  const is_required_auth = to.matched.some(route => route.meta.auth)
  if (is_required_auth && !access_token) {
    next({
      path: '/auth',
      query: {
        callback: to.fullPath
      }
    })
    return
  }
  next()
})

Vue.mixin({
  computed: {
    ...mapGetters('Common', ['common_data', 'common_utils']),
    ...mapGetters('User', ['auth_user', 'access_token']),
    ...mapState('Device', {
      device_screen_size: 'screen_size',
      locale: 'locale'
    }),
  }
})

new Vue({
  router,
  store,
  created () {
    this.$services.context = this
  },
  render: h => h(App)
}).$mount('#app')


