export default {
  namespaced: true,
  state: {
    auth_user: {},
    access_token: null,
    signing_in: false,
    sign_in_type: null,
  },
  mutations: {
    signed_in (state, {access_token, auth_user}) {
      state.access_token = access_token
      Object.assign(state.auth_user, auth_user)
    },
    signed_out (state) {
      state.access_token = null
      state.auth_user = {}
    },
    signing_in (state, value) {
      state.signing_in = value
    },
    sign_in_type (state, value) {
      state.sign_in_type = value
    },
    auth_user (state, data) {
      state.auth_user = data
    }
  },
  actions: {
    start_signing_in ({commit}, type) {
      commit('signing_in', true)
      commit('sign_in_type', type)
    },
    stop_signing_in ({commit}) {
      commit('signing_in', false)
      commit('sign_in_type', null)
    }
  },
  getters: {
    auth_user: state => state.auth_user,
    access_token: state => state.access_token
  }
}
