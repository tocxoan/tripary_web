import moment from 'moment/src/moment'

export default {
  namespaced: true,
  state: {
    data: {
      max_content_length: 15 * 1024 * 1024,
      date_format: 'DD/MM/YYYY',
      email_regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      place_types: [
        {
          id: 'lodging',
          name: 'Chỗ ở'
        },
        {
          id: 'restaurant',
          name: 'Nhà hàng'
        }
      ],
      file_ext_icons: {
        'doc': require('@/assets/file-ext-icons/doc.svg'),
        'pdf': require('@/assets/file-ext-icons/pdf.svg'),
        'xls': require('@/assets/file-ext-icons/xls.svg'),
        'xlsx': require('@/assets/file-ext-icons/xls.svg'),
        'other': require('@/assets/file-ext-icons/other.svg'),
      },
      locales: [
        // https://www.iconfinder.com/iconsets/142-mini-country-flags-16x16px
        {
          id: 'vi',
          logo: require('@/assets/locales/vi.png')
        },
        {
          id: 'en',
          logo: require('@/assets/locales/us.png')
        }
      ]
    },
    utils: {
      convert_string_to_date: (string, format) => {
        if (!string) return null
        return moment(string, format)
      },
      convert_date_format: (string, from_format, to_format) => {
        if (!string) return null
        const datetime = moment(string, from_format)
        return datetime.format(to_format)
      },
      get_current_month_to_now: (format) => {
        if (!format) format = 'YYYY-MM-DD'
        let now = moment()
        let start_of_month = now.clone().startOf('month')
        return [start_of_month.format(format), now.format(format)]
      },
      get_window_size: window_width => {
        if (window_width > 1904) return 'xl'
        if (window_width > 1264) return 'lg'
        if (window_width > 960) return 'md'
        if (window_width > 600) return 'sm'
        return 'xs'
      },
      dive_to_path (path, data) {
        let root = path.split('.')
        const dive = (_data) => {
          let result = _data
          if (root.length > 0) {
            let key = root.splice(0, 1)
            return dive(result[key])
          }
          return result
        }
        return dive(data)
      },
      get_object_translation (locale, object) {
        let trans = object.translations || []
        return trans.find(t => t.locale === locale)
      }
    },
    constants_loaded: null
  },
  mutations: {
    loading_constants_success: (state, data) => {
      Object.assign(state.data, data)
      state.constants_loaded = true
    },
    constants_loaded: (state, value) => {
      state.constants_loaded = value
    }
  },
  actions: {},
  getters: {
    common_data: state => state.data,
    common_utils: state => state.utils
  }
}
