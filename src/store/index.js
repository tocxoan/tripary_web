/**
 * Created by shin on 22/06/2017.
 */
import Vuex from 'vuex'
import Vue from 'vue'

import createPersistedState from 'vuex-persistedstate'

import User from './user'
import Common from './common'
import Device from './device'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    User,
    Device,
    Common
  },
  plugins: [createPersistedState({
    key: process.env.VUE_APP_VUEX_KEY,
    reducer (state) {
      return {
        User: {
          access_token: state.User.access_token,
          auth_user: state.User.auth_user
        },
        Device: {
          locale: state.Device.locale
        }
      }
    }
  })],
  strict: process.env.NODE_ENV !== 'production'
})
