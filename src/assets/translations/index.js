export default {
  common: {
    close: {
      vi: 'Đóng',
      en: 'Close'
    },
    share: {
      vi: 'Chia sẻ',
      en: 'Share'
    },
    sign_in: {
      vi: 'Đăng nhập',
      en: 'Sign in'
    },
    group_list: {
      vi: 'Danh sách nhóm',
      en: 'Group list'
    },
    brand: {
      vi: 'Hãng',
      en: 'Brand'
    },
    flight_number: {
      vi: 'Mã chuyến bay',
      en: 'Flight number'
    },
    booking_number: {
      vi: 'Mã đặt chỗ',
      en: 'Booking number'
    },
    room_type: {
      vi: 'Loại phòng',
      en: 'Room type'
    },
    view_map: {
      vi: 'Xem map',
      en: 'View map'
    },
    day_list: {
      vi: 'Danh sách ngày',
      en: 'Day list'
    },
    search: {
      vi: 'Tìm kiếm',
      en: 'Search'
    },
    day: {
      vi: 'Ngày',
      en: 'Day'
    },
    event: {
      vi: 'Sự kiện',
      en: 'Event'
    },
    detail: {
      vi: 'Chi tiết',
      en: 'Detail'
    },
    edit: {
      vi: 'Sửa',
      en: 'Edit'
    },
    copy: {
      vi: 'Sao chép',
      en: 'Copy'
    },
    remove: {
      vi: 'Xóa',
      en: 'Remove'
    },
    settings: {
      vi: 'Cài đặt',
      en: 'Settings'
    },
    backward: {
      vi: 'Quay lại',
      en: 'backward'
    },
    save: {
      vi: 'Lưu',
      en: 'Save'
    },
    submit: {
      vi: 'Chấp nhận',
      en: 'Submit'
    },
    cancel: {
      vi: 'Hủy',
      en: 'Cancel'
    },
    view: {
      vi: 'Xem',
      en: 'View'
    },
    start_date: {
      vi: 'Khởi hành',
      en: 'Start date'
    },
    desc: {
      vi: 'Mô tả',
      en: 'Description'
    },
    at: {
      vi: 'Tại',
      en: 'At'
    },
    to: {
      vi: 'Đến',
      en: 'To'
    },
    departure: {
      vi: 'Khởi hành',
      en: 'Departure'
    },
    arrive: {
      vi: 'Đến',
      en: 'Arrive'
    },
    image: {
      vi: 'Hình ảnh',
      en: 'Image'
    },
    maximum: {
      vi: 'Tối đa',
      en: 'Maximum'
    },
    attachment: {
      vi: 'File đính kèm',
      en: 'Attachment'
    },
    public: {
      vi: 'Công khai',
      en: 'Public'
    },
    download: {
      vi: 'Tải về',
      en: 'Download'
    },
    yesterday: {
      vi: 'Hôm qua',
      en: 'Yesterday'
    },
    when: {
      vi: 'Lúc',
      en: 'When'
    },
    add: {
      vi: 'Thêm',
      en: 'Add'
    },
    itinerary: {
      vi: 'Lịch trình',
      en: 'Itinerary'
    },
    member: {
      vi: 'Thành viên',
      en: 'Member'
    },
    comment: {
      vi: 'Bình luận',
      en: 'Comment'
    },
    group: {
      vi: 'Nhóm',
      en: 'Group'
    },
    notification: {
      vi: 'Thông báo',
      en: 'Notification'
    },
    view_more: {
      vi: 'Xem thêm',
      en: 'View more'
    },
    by: {
      vi: 'Bởi',
      en: 'By'
    }
  },
  event_categories: {
    cuisine: {
      vi: 'Ăn uống',
      en: 'Dining'
    },
    travel: {
      vi: 'Di chuyển',
      en: 'Transportation'
    },
    stay: {
      vi: 'Chỗ ở',
      en: 'Accommodation'
    },
    activity: {
      vi: 'Hoạt động',
      en: 'Activity'
    }
  },
  event_sub_categories: {
    'check-in': {
      vi: 'Checkin',
      en: 'Checkin'
    },
    'check-out': {
      vi: 'Checkout',
      en: 'Checkout'
    },
    'break-fast': {
      vi: 'Bữa sáng',
      en: 'Breakfast'
    },
    'lunch': {
      vi: 'Bữa trưa',
      en: 'Lunch'
    },
    'dinner': {
      vi: 'Bữa tối',
      en: 'Dinner'
    },
    'snack': {
      vi: 'Bữa phụ',
      en: 'Snack'
    },
    'flight': {
      vi: 'Máy bay',
      en: 'Flight'
    },
    'train': {
      vi: 'Tàu hỏa',
      en: 'Train'
    },
    'car': {
      vi: 'Oto',
      en: 'Car'
    },
    'coach': {
      vi: 'Xe khách',
      en: 'Coach'
    },
    'motorbike': {
      vi: 'Xe máy',
      en: 'Motorbike'
    },
    'bicycle': {
      vi: 'Xe đạp',
      en: 'Bicycle'
    },
    'walk': {
      vi: 'Đi bộ',
      en: 'Walk'
    },
    'others': {
      vi: 'Khác',
      en: 'Others'
    },
    'movie': {
      vi: 'Xem phim',
      en: 'Cinema'
    },
    'sight-seeing': {
      vi: 'Tham quan',
      en: 'Sight seeing'
    },
    'park': {
      vi: 'Công viên',
      en: 'Park'
    },
    'street-walk': {
      vi: 'Dạo phố',
      en: 'Street view'
    },
    'bar': {
      vi: 'Bar/Club',
      en: 'Bar/Club'
    },
    'team-building': {
      vi: 'Team building',
      en: 'Team building'
    }
  },
  notification: {
    read_all: {
      vi: 'Đánh dấu tất cả là đã đọc',
      en: 'Mark all as read'
    }
  },
  comment: {
    input_placeholder: {
      vi: 'Viết bình luận...',
      en: 'Write your comment...'
    },
    some_second_ago: {
      vi: 'giây trước',
      en: 'seconds ago'
    },
    some_minute_ago: {
      vi: 'phút trước',
      en: 'minutes ago'
    },
    some_hour_ago: {
      vi: 'tiếng trước',
      en: 'hours ago'
    }
  },
  itinerary: {
    period: {
      vi: 'Thời gian',
      en: 'Period'
    },
    edit_it_info: {
      vi: 'Sửa thông tin lịch trình',
      en: 'Edit itinerary info'
    },
    it_name: {
      vi: 'Tên lịch trình',
      en: 'Itinerary name'
    },
    change_cover_image: {
      vi: 'Đổi hình đại diện',
      en: 'Change cover image'
    },
    event_note_placeholder: {
      vi: 'Chú thích cho sự kiện này',
      en: 'Write something about this event'
    },
    image_desc_placeholder: {
      vi: 'Hãy viết điều gì đó về ảnh này',
      en: 'Write something about this photo'
    },
    image_status_info: {
      vi: 'Tắt "Công khai" nếu bạn không muốn người khác nhìn thấy ảnh này khi công khai lịch trình',
      en: 'Disable "Public" if you dont want other people see this photo'
    },
    has_desc: {
      vi: 'Ảnh này có mô tả',
      en: 'This image has description'
    },
    private_state: {
      vi: 'Ảnh này  đang ở chế độ riêng tư',
      en: 'This image is private'
    },
    public_state: {
      vi: 'Ảnh này đang ở chế độ công khai',
      en: 'This image is public'
    },
    moved_event: {
      vi: 'Sự kiện đang được chuyển',
      en: 'Moved event'
    },
    time_and_place: {
      vi: 'Thời gian và địa điểm',
      en: 'When and where'
    },
    edit_event: {
      vi: 'Sửa sự kiên',
      en: 'Edit event'
    },
    error_file: {
      vi: 'File lỗi',
      en: 'Error file'
    },
    add_event: {
      vi: 'Thêm sự kiện',
      en: 'Add event'
    },
    has_no_place: {
      vi: 'Chưa có địa điểm nào được chọn',
      en: 'This day has no place yet'
    },
    day_overview: {
      vi: 'Tóm tắt ngày',
      en: 'Day overview'
    },
    delete_day: {
      vi: 'Xóa ngày',
      en: 'Remove day'
    },
    has_no_event: {
      vi: 'Ngày hôm nay không có sự kiện nào',
      en: 'There is no event today'
    },
    set_day_title: {
      vi: 'Đặt title cho ngày',
      en: 'Set today title'
    },
    next_day: {
      vi: 'Ngày tiếp theo',
      en: 'Next day'
    },
    previous_day: {
      vi: 'Ngày trước đó',
      en: 'Previous day'
    },
    add_day: {
      vi: 'Thêm ngày',
      en: 'Add day'
    },
    ongoing: {
      vi: 'Đang trải nghiệm',
      en: 'Ongoing'
    },
    planning: {
      vi: 'Lên kế hoạch',
      en: 'Planning'
    },
    finished: {
      vi: 'Đã kết thúc',
      en: 'Finished'
    },
    all: {
      vi: 'Tất cả',
      en: 'All'
    },
    create_it: {
      vi: 'Tạo lịch trình',
      en: 'Create itinerary'
    },
    it_not_found: {
      vi: 'Không tìm thấy lịch trình nào',
      en: 'No results found'
    },
    has_no_it: {
      vi: 'Chưa có lịch trình nào',
      en: 'You dont have any itinerary'
    },
    no_fit_result: {
      vi: 'Không tìm thấy lịch trình nào phù hợp với',
      en: 'No results found with'
    },
    of_group: {
      vi: 'Của nhóm',
      en: 'Of group'
    },
    remove_confirmation: {
      vi: 'Bạn có chắc chắn muốn xóa lịch trình này?',
      en: 'Are you sure to remove?'
    },
    public_it_list: {
      vi: 'Lịch trình công khai',
      en: 'Other public itineraries'
    },
    clone_confirmation: {
      vi: 'Bạn có chắc chắn muốn sao chép lịch trình này?',
      en: 'Are you sure to copy this itinerary?'
    },
    clone_is_in_progress: {
      vi: 'Đang sao chép lịch trình ...',
      en: 'Itinerary copying ...'
    },
    settings: {
      who_can_see: {
        vi: 'Ai có thể nhìn thấy lịch trình này?',
        en: 'Who can see this itinerary?'
      },
      event_suggestion: {
        vi: 'Gợi ý cho sự kiện',
        en: 'Event suggestion'
      },
      type_name_or_email: {
        vi: 'Nhập tên, email',
        en: 'Name, email'
      },
      type_group_name: {
        vi: 'Nhập tên nhóm',
        en: 'Group name'
      },
      statuses: {
        private: {
          vi: 'Chỉ mình tôi',
          en: 'Only me'
        },
        share: {
          vi: 'User hoặc nhóm',
          en: 'User or group'
        },
        public: {
          vi: 'Công khai',
          en: 'Public'
        }
      }
    },
    errors: {
      ItineraryNotFound: {
        vi: 'Lịch trình này không tồn tại hoặc đã bị xóa',
        en: 'Itinerary not found or deleted'
      }
    },
    move_event: {
      vi: 'Chuyển sự kiện',
      en: 'Move event'
    },
    delete_event: {
      vi: 'Xóa sự kiện',
      en: 'Delete event'
    },
    attachment_status_info: {
      vi: 'Tắt "Công khai" nếu bạn không muốn người khác nhìn thấy file này khi công khai lịch trình',
      en: 'Disable "Public" if you dont want other people see this file'
    }
  },
  storage: {
    errors: {
      ContentTypeNotSupported: {
        vi: 'Định dạng file không hợp lệ',
        en: 'File content type is not supported'
      }
    }
  },
  event_discussion: {
    no_suggestion: {
      vi: 'Chưa có địa điểm nào được gợi ý',
      en: 'There is no place suggestion'
    },
    place_suggestion: {
      vi: 'Gợi ý địa điểm',
      en: 'Suggest place'
    },
    select_as_departure: {
      vi: 'Chọn làm điểm khởi hành',
      en: 'Select as departure place'
    },
    select_as_arrival: {
      vi: 'Chọn làm điểm kết thúc',
      en: 'Select as arrival place'
    },
    select_as_destination: {
      vi: 'Chọn làm điểm đến',
      en: 'Select as destination'
    },
    arrival: {
      vi: 'Kết thúc',
      en: 'Arrival'
    }
  },
  group: {
    add_group: {
      vi: 'Thêm nhóm',
      en: 'Add group'
    },
    no_group: {
      vi: 'Chưa có nhóm nào',
      en: 'There isnt any group yet'
    },
    leaving_confirmation: {
      vi: 'Bạn chắc chắn muốn rời khỏi nhóm?',
      en: 'Are you sure?'
    },
    group_name: {
      vi: 'Tên nhóm',
      en: 'Group name'
    },
    edit_group: {
      vi: 'Sửa thông tin nhóm',
      en: 'Edit group'
    }
  },
  errors: {
    e403: {
      vi: 'Bạn không có quyền truy cập vào trang này',
      en: 'Permission error'
    }
  },
  landing: {
    text_1: {
      vi: 'Đi cùng bạn, đi muôn nơi',
      en: 'Be with you everywhere at anytime'
    },
    text_2: {
      vi: 'Nơi các nhóm du lịch có thể lên kế hoạch, cùng bàn luận và chọn cho mình những địa điểm thú vị nhất để trải nghiệm',
      en: 'Where tour groups can plan, discuss and choose for themselves the most interesting places to experience'
    },
    text_3: {
      vi: 'Chưa bao giờ lên lịch trình dễ dàng đến thế',
      en: 'Easy to create a trip',
    },
    create_it_now: {
      vi: 'Tạo lịch trình ngay',
      en: 'Create itinerary now'
    },
    start_now: {
      vi: 'Bắt đầu ngay',
      en: 'Start now'
    },
    step_titles: {
      sharing: {
        vi: 'Chia sẻ',
        en: 'Sharing'
      },
      ongoing: {
        vi: 'Trải nghiệm',
        en: 'Ongoing'
      },
      planning: {
        vi: 'Lên kế hoạch',
        en: 'Planning'
      }
    },
    steps: {
      planning: {
        vi: 'Dễ dàng lên kế hoạch với những lịch trình và hoạt động được gợi tý từ hệ thống. Thuận tiện để thảo luận và lựa chọn các địa điểm phù hợp với sở thích của nhóm.',
        en: 'Easily plan with suggested schedules and activities from the system. Convenient to discuss and select locations that match the interests of the group.'

      },
      ongoing: {
        vi: 'Theo dõi lịch trình của cả nhóm với hệ thống nhác lịch hoàn toàn tự động. Cả nhóm sẽ luôn có những trải nghiệm thú vị cùng với nhau.',
        en: 'Keep track of the whole group schedule with the fully automated calendar system. The whole group will always have interesting experiences together.'
      },
      sharing: {
        vi: 'Lưu lại kỉ niệm những chuyến đi tuyệt vời của nhóm và chia sẻ cùng bạn bè, người thân để nhân đôi niềm vui.',
        en: 'Save memories of the wonderful trips of the group and share with friends and relatives to double the joy.'
      },
    },
  }
}
