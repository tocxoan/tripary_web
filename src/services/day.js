export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/day/list', request)
  }

  list_place (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/day/place/list', request)
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/day', request)
  }

  update (payload) {
    let request = {
      method: 'patch',
      payload
    }
    return this.base.do_request('/day', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/day', request)
  }
}
