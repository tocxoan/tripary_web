export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/share/list', request)
  }
  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/share', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/share', request)
  }

  handle_share_url (payload) {
    let request = {
      method: 'put',
      payload
    }
    return this.base.do_request('/share', request)
  }
}
