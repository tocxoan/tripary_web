export default class Services {
  constructor (base) {
    this.ERRORS = {
      DuplicateSuggestion: 'Địa điểm này đã được gợi ý rồi'
    }
    this.base = base
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/place/suggestion', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/place/suggestion', request)
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/place/suggestion/list', request)
  }
}
