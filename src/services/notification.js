export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/notification/list', request)
  }

  read (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/notification/read', request)
  }

  read_all () {
    let request = {
      method: 'post'
    }
    return this.base.do_request('/notification/read-all', request)
  }

  count_unseen () {
    let request = {
      method: 'get',
      payload: {}
    }
    return this.base.do_request('/notification/unseen/count', request)
  }

}
