export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  get () {
    let request = {
      method: 'get'
    }
    return this.base.do_request('/entrance', request)
  }
}
