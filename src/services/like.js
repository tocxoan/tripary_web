export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/like', request)
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/like/list', request)
  }

}
