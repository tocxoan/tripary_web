export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  fb_auth (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/user/fb-auth', request)
  }

  gg_auth (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/user/gg-auth', request)
  }
  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/user/list', request)
  }
  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/user', request)
  }
}
