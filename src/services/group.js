export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/group/list', request)
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/group', request)
  }

  update (payload) {
    let request = {
      method: 'patch',
      payload
    }
    return this.base.do_request('/group', request)
  }

  add_member (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/group/member', request)
  }

  list_member (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/group/member/list', request)
  }

  remove_member (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/group/member', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/group', request)
  }

  confirm_invitation (id) {
    let request = {
      method: 'post',
      payload: {id}
    }
    return this.base.do_request('/group/invitation/confirm', request)
  }

}
