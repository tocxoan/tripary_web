export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/comment/list', request)
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/comment', request)
  }
}
