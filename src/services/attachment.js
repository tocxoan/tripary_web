export default class Services {
  constructor (base) {
    this.ERRORS = {
      'ContentTypeNotSupported': 'Định dạng file không hợp lệ.'
    }
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/attachment/list', request)
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/attachment', request)
  }

  update (payload) {
    let request = {
      method: 'patch',
      payload
    }
    return this.base.do_request('/attachment', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/attachment', request)
  }

}
