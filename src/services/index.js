/**
 * Created by shin on 22/06/2017.
 */
import Axios from 'axios'

import UserServices from './user'
import EntranceServices from './entrance'
import ItineraryServices from './itinerary'
import DayServices from './day'
import EventServices from './event'
import ImageServices from './image'
import ShareServices from './share'
import GroupServices from './group'
import MessageServices from './message'
import CommentServices from './comment'
import PlaceServices from './place'
import PlaceSuggestionServices from './place_suggestion'
import LikeServices from './like'
import AttachmentServices from './attachment'
import NotificationServices from './notification'


const axios = Axios.create({
  // set default BACKEND_API
  baseURL: process.env.VUE_APP_API_URL || 'http://127.0.0.1:5000',
  withCredentials: true
})

class Services {
  constructor () {
    this.Axios = Axios
    this.context = null
    this.user = new UserServices(this)
    this.entrance = new EntranceServices(this)
    this.itinerary = new ItineraryServices(this)
    this.day = new DayServices(this)
    this.event = new EventServices(this)
    this.image = new ImageServices(this)
    this.attachment = new AttachmentServices(this)
    this.share = new ShareServices(this)
    this.group = new GroupServices(this)
    this.message = new MessageServices(this)
    this.comment = new CommentServices(this)
    this.place = new PlaceServices(this)
    this.place_suggestion = new PlaceSuggestionServices(this)
    this.like = new LikeServices(this)
    this.notification = new NotificationServices(this)
  }

  make_request_data ({method, payload, headers = null, handle_progress = null}) {
    let full_headers = this.get_auth_header()
    if (headers) {
      Object.assign(full_headers, headers)
    }
    if (method === 'get' || method === 'delete') {
      return [{
        headers: full_headers,
        params: payload
      }]
    } else if (method === 'post' || method === 'put' || method === 'patch') {
      const options = {}
      if (typeof handle_progress === 'function') options['onUploadProgress'] = handle_progress
      return [
        payload,
        {headers: full_headers, ...options}
      ]
    }
  }

  start_request (endpoint, request) {
    // make the proper data for method before requesting
    let request_data = this.make_request_data(request)

    // do request
    return axios[request.method](endpoint, ...request_data).then(
      response => response
    ).catch(
      error => error.response
    )
  }

  end_request (response, request) {
    if (!response) response = {status: 500}
    if (this.context && response.status === 401 && !request.ignore_401) {
      this.context.$store.commit('User/signed_out')
      this.context.$router.push('/auth')
      return response
    }
    return response
  }

  async do_request (endpoint, request) {
    if (!request.payload || !(request.payload instanceof Object)) request.payload = {}

    // start request
    const response = await this.start_request(endpoint, request)
    if (process.env.NODE_ENV !== 'production') console.dir(response)

    // end request
    return this.end_request(response, request)
  }

  get_auth_header () {
    if (!process.browser) return {}
    if (!this.context) return {}
    return {
      Authorization: 'Bearer ' + this.context.access_token
    }
  }
}

const services = new Services()

export default {
  install: (Vue, options) => {
    Vue.prototype.$services = services
  }
}
