export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/place', request)
  }

  get (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/place', request)
  }
}
