export default class Services {
  constructor (base) {
    this.ERRORS = {}
    this.base = base
  }

  create () {
    let request = {
      method: 'post'
    }
    return this.base.do_request('/itinerary', request)
  }

  update (payload) {
    let request = {
      method: 'patch',
      payload
    }
    return this.base.do_request('/itinerary', request)
  }

  get (id) {
    let request = {
      method: 'get',
      payload: {id}
    }
    return this.base.do_request('/itinerary', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/itinerary', request)
  }

  clone (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/itinerary/clone', request)
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/itinerary/list', request)
  }

}
