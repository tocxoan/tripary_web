export default class Services {
  constructor (base) {
    this.ERRORS = {
      'ContentTypeNotSupported': 'Định dạng file không hợp lệ.'
    }
    this.base = base
  }

  list (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/image/list', request)
  }

  get_default (payload) {
    let request = {
      method: 'get',
      payload
    }
    return this.base.do_request('/image/default', request)
  }

  create (payload) {
    let request = {
      method: 'post',
      payload
    }
    return this.base.do_request('/image', request)
  }

  update (payload) {
    let request = {
      method: 'patch',
      payload
    }
    return this.base.do_request('/image', request)
  }

  delete (payload) {
    let request = {
      method: 'delete',
      payload
    }
    return this.base.do_request('/image', request)
  }

}
