import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      name: 'auth',
      path: '/auth',
      component: () => import('./views/auth')
    },
    {
      name: 'app',
      path: '/app',
      component: () => import('./views/app'),
      children: [
        {
          name: 'view.itinerary',
          path: 'view/itinerary/:id',
          component: () => import('./views/app/view/itinerary'),
        },
        {
          path: 'cp',
          name: 'control-panel',
          redirect: '/app/cp/itinerary',
          component: () => import('./views/app/control-panel'),
          meta: {
            auth: true
          },
          children: [
            {
              name: 'group',
              path: 'group',
              component: () => import('./views/app/control-panel/group'),
            },
            {
              path: 'itinerary',
              name: 'itinerary.list',
              component: () => import('./views/app/control-panel/itinerary/list'),
            },
            {
              path: 'itinerary/:id',
              name: 'itinerary.edit',
              component: () => import('./views/app/control-panel/itinerary/edit'),
            }
          ]
        }
      ]
    },
    {
      name: 'introduction',
      path: '/',
      component: () => import('./views/landing')
    },
    {
      name: 'group.invitation',
      path: '/group-invitation/:id',
      component: () => import('./views/invitation/group'),
      meta: {
        auth: true
      }
    },
    {
      name: 'itinerary.share',
      path: '/itinerary-share/:id',
      component: () => import('./views/share/itinerary'),
      meta: {
        auth: true
      }
    }
  ]
})
