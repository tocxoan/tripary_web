import * as firebase from 'firebase/app'

const FIREBASE_CONFIG = {
  apiKey: "AIzaSyBTblxtIOyLPdF1tUY2LGmQqNRyZA2pgHE",
  authDomain: "tripary.firebaseapp.com",
  databaseURL: "https://tripary.firebaseio.com",
  projectId: "tripary",
  storageBucket: "tripary.appspot.com",
  messagingSenderId: "711292779783",
  appId: "1:711292779783:web:14f56b0d59bdece7"
}

export default {
  install: (Vue, options) => {
    firebase.initializeApp(FIREBASE_CONFIG)
    Vue.prototype.$firebase = firebase
  }
}
