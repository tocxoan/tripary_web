/* eslint-disable one-var */
const load_js = () => {
  const script_id = 'facebook-jssdk'
  if (document.getElementById(script_id)) {
    return
  }
  let js = document.createElement('script')
  js.id = script_id
  js.src = 'https://connect.facebook.net/en_US/sdk.js'
  document.getElementsByTagName('head')[0].appendChild(js)
}

export default {
  install: (Vue, options) => {
    load_js(document, 'script', 'facebook-jssdk')
    window.fbAsyncInit = () => {
      window.FB.init({
        xfbml: true,
        autoLogAppEvents: true,
        version: 'v3.0',
        appId: process.env.VUE_APP_FACEBOOK_APP_ID,
        ...options
      })
      window.FB.AppEvents.logPageView()
      Vue.prototype.$facebook = window.FB
      console.log('FB sdk is ready!')
    }
    Vue.prototype.$facebook = null
  }
}
