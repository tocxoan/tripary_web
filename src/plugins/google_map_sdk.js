import loadGoogleMapsAPI from 'load-google-maps-api'

export default {
  install: (Vue, options) => {
    Vue.prototype.$google_map = null
    loadGoogleMapsAPI({
      key: process.env.VUE_APP_GOOGLE_API_KEY,
      language: 'vi',
      libraries: ['places'],
      ...options
    }).then(google => {
      Vue.prototype.$google_map = google
      console.log('Google map sdk is ready!')
    })
  }
}
