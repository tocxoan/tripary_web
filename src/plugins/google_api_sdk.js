/* eslint-disable one-var */
const load_js = () => {
  const script_id = 'google-api-sdk'
  if (document.getElementById(script_id)) {
    return
  }
  let js = document.createElement('script')
  js.id = script_id
  js.src = 'https://apis.google.com/js/platform.js'
  document.getElementsByTagName('head')[0].appendChild(js)
}

export default {
  install: (Vue, options) => {
    Vue.prototype.$gapi = null
    load_js()
    let interval = setInterval(() => {
      if (window.gapi) {
        let gapi = window.gapi
        gapi.load('auth2', function () {
          gapi.auth2.init({
            client_id: process.env.VUE_APP_GOOGLE_AUTH2_CLIENT_ID,
            scope: 'profile'
          })
        })
        Vue.prototype.$gapi = gapi
        clearInterval(interval)
      }
    }, 100)
  }
}
