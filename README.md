# Tripary web

## Requirement
Nodejs 6.9.0+

## Configuration
Create .env file in project directory 

```.env
VUE_APP_VUEX_KEY=tripary
VUE_APP_API_URL=https://tripary.me/api
VUE_APP_FACEBOOK_APP_ID=facebook_app_id
VUE_APP_WEB_URL=http://localhost
VUE_APP_GOOGLE_API_KEY=api_key
VUE_APP_GOOGLE_AUTH2_CLIENT_ID=client_id
```

## Setup
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
