const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  configureWebpack: {
    plugins: [new BundleAnalyzerPlugin()],
    resolve: {
      alias: {
        moment: 'moment/src/moment'
      }
    }
  },
  pwa: {
    name: 'Tripary',
    appleMobileWebAppCapable: 'yes',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'public/service-worker.js',
    }
  }
}